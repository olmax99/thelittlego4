package thelittlego4

import (
	"gitlab.com/olmax99/thelittlego4/db"
)

func PriceCheck(itemId int) (float64, bool) {
	item := db.LoadItem(itemId)
	if item == nil {
		return 0, false
	}
	return item.Price, true
}
