package thelittlego4

import (
	"testing"
)

var Results = []struct {
	in   int
	out1 float64
	out2 bool
}{
	{1111, 9.001, true},
}

func TestPriceCheck(t *testing.T) {
	for _, tt := range Results {
		o1, o2 := PriceCheck(tt.in)
		if o1 != tt.out1 || o2 != tt.out2 {
			// t.Errorf("PriceCheck(%d) => %d, %v, want %d, %v", tt.in, s, t, tt.out1, tt.out2)
			t.Errorf("PriceCheck(%d) =>  %f %t , want %f %t", tt.in, tt.out1, tt.out2, o1, o2)
		}
	}
}
